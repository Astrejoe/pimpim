const cellSize = 100;
const operator = {
    nothing: -1,
    plus: 0,
    minus: 1,
}
const direction = {
    up: 0,
    down: 1,
    left: 2,
    right: 3
}
const height = 5;
const width = 5;
const modulo = 10;
const numbers = [];
const path = [];
const operators = [];
const visited = [];
const currentPosition = {
    x: 0,
    y: 0
}
const cellDoms = [];
let targetScore = 0;
let score = 0;
let robotPlaying = false;

document.addEventListener('keydown', onKeyDown);

function onKeyDown(e) {
    switch (e.code) {
        case "ArrowUp":
            move(direction.up)
            break
        case "ArrowDown":
            move(direction.down)
            break
        case "ArrowLeft":
            move(direction.left)
            break
        case "ArrowRight":
            move(direction.right)
            break
        default:
            break
    }
}

function move(dir) {
    switch (dir) {
        case direction.up: {
            const newY = Math.max(currentPosition.y - 1, 0)
            if(path.length > 0 && path[path.length - 1] === direction.down) {
                path.pop();
                visited[currentPosition.x][currentPosition.y] = false
                currentPosition.y = newY
            } else if(!visited[currentPosition.x][newY]) {
                currentPosition.y = newY
                path.push(direction.up)
                visited[currentPosition.x][newY] = true;
            }
            break
        }
        case direction.down: {
            const newY = Math.min(currentPosition.y + 1, height - 1)
            if(path.length > 0 && path[path.length - 1] === direction.up) {
                path.pop();
                visited[currentPosition.x][currentPosition.y] = false
                currentPosition.y = newY
            } else if(!visited[currentPosition.x][newY]) {
                currentPosition.y = newY
                path.push(direction.down)
                visited[currentPosition.x][newY] = true;
            }
            break
        }
        case direction.left: {
            const newX = Math.max(currentPosition.x - 1, 0)
            if(path.length > 0 && path[path.length - 1] === direction.right) {
                path.pop();
                visited[currentPosition.x][currentPosition.y] = false
                currentPosition.x = newX
            } else if(!visited[newX][currentPosition.y]) {
                currentPosition.x = newX
                path.push(direction.left)
                visited[newX][currentPosition.y] = true;
            }
            break
        }
        case direction.right: {
            const newX = Math.min(currentPosition.x + 1, width - 1)
            if(path.length > 0 && path[path.length - 1] === direction.left) {
                path.pop();
                visited[currentPosition.x][currentPosition.y] = false
                currentPosition.x = newX
            } else if(!visited[newX][currentPosition.y]) {
                currentPosition.x = newX
                path.push(direction.right)
                visited[newX][currentPosition.y] = true;
            }
            break
        }
        default:
            break
    }
    draw();
}

function firstDraw() {
    const currentPositionDom = document.querySelector('#current-position')
    currentPositionDom.style.left = `${currentPosition.x * cellSize}px`;
    currentPositionDom.style.top = `${currentPosition.y * cellSize}px`;

    const boardDom = document.querySelector('#board')
    const cellContainerDom = document.querySelector('#cell-container')
    cellContainerDom.innerHTML = ''
    boardDom.style.width = `${width * cellSize}px`
    boardDom.style.height = `${height * cellSize}px`
    numbers.forEach((column, xIndex) => {
        cellDoms.push([])
        column.forEach((cellNumber, yIndex) => {
            const cellDom = document.createElement('div')
            const cellContentsDom = document.createTextNode(cellNumber)
            cellDom.appendChild(cellContentsDom)
            cellDom.className = "cell"
            cellDom.id = positionToString("cell", xIndex, yIndex)
            cellDom.style.left = `${xIndex * cellSize}px`
            cellDom.style.top = `${yIndex * cellSize}px`
            cellContainerDom.appendChild(cellDom)
            cellDoms[xIndex].push(cellDom)
            if(xIndex === width - 1 && yIndex === height - 1) {
                cellDom.className = "cell final-cell"
            }
        })
    })

    const operatorContainerDom = document.querySelector('#operator-container')
    operatorContainerDom.innerHTML = ''
    operators.forEach((column, xIndex) => {
        column.forEach((op, yIndex) => {
            if(op !== operator.nothing) {
                const operatorDom = document.createElement('div')
                const operatorContentsDom = document.createTextNode(op === operator.plus ? "+" : "-")
                operatorDom.appendChild(operatorContentsDom)
                operatorDom.className = "operator"
                operatorDom.id = positionToString("operator", xIndex, yIndex)
                operatorDom.style.left = `${xIndex * cellSize / 2 + cellSize / 4}px`
                operatorDom.style.top = `${yIndex * cellSize / 2 + cellSize / 4}px`
                operatorContainerDom.appendChild(operatorDom)
            }
        })
    })

    const pathContainerDom = document.querySelector('#path-container')
    pathContainerDom.innerHTML = ''
}

function draw() {
    const currentPositionDom = document.querySelector('#current-position')
    currentPositionDom.style.left = `${currentPosition.x * cellSize}px`;
    currentPositionDom.style.top = `${currentPosition.y * cellSize}px`;
    cellDoms.forEach((column, xIndex) => {
        column.forEach((cellDom, yIndex) => {
            if(visited[xIndex][yIndex]) {
                cellDom.className = 'cell visited'
            } else {
                cellDom.className = 'cell'
                if(xIndex === width - 1 && yIndex === height - 1) {
                    cellDom.className = "cell final-cell"
                }
            }
        })
    })

    const pathContainerDom = document.querySelector('#path-container')
    let pathPosition = {
        x: 0,
        y: 0
    }
    pathContainerDom.innerHTML = ''
    score = numbers[0][0]
    path.forEach(dir => {
        const pathPieceDom = document.createElement('div')
        pathPieceDom.className = "path-piece"
        let nextPosition = {
            x: pathPosition.x,
            y: pathPosition.y
        }
        const widthOrHeight = '5px'
        switch (dir) {
            case direction.up:
                nextPosition.y -= 1
                pathPieceDom.style.width = widthOrHeight
                pathPieceDom.style.height = `${cellSize}px`
                pathPieceDom.style.left = `${pathPosition.x * cellSize + cellSize / 2}px`
                pathPieceDom.style.top = `${nextPosition.y * cellSize + cellSize / 2}px`
                break
            case direction.down:
                nextPosition.y += 1
                pathPieceDom.style.width = widthOrHeight
                pathPieceDom.style.height = `${cellSize}px`
                pathPieceDom.style.left = `${pathPosition.x * cellSize + cellSize / 2}px`
                pathPieceDom.style.top = `${pathPosition.y * cellSize + cellSize / 2}px`
                break
            case direction.left:
                nextPosition.x -= 1
                pathPieceDom.style.height = widthOrHeight
                pathPieceDom.style.width = `${cellSize}px`
                pathPieceDom.style.left = `${nextPosition.x * cellSize + cellSize / 2}px`
                pathPieceDom.style.top = `${pathPosition.y * cellSize + cellSize / 2}px`
                break
            case direction.right:
                nextPosition.x += 1
                pathPieceDom.style.height = widthOrHeight
                pathPieceDom.style.width = `${cellSize}px`
                pathPieceDom.style.left = `${pathPosition.x * cellSize + cellSize / 2}px`
                pathPieceDom.style.top = `${pathPosition.y * cellSize + cellSize / 2}px`
                break
            default:
                break
        }
        pathContainerDom.appendChild(pathPieceDom)
        const op = operators[nextPosition.x + pathPosition.x][nextPosition.y + pathPosition.y];
        if(op === operator.plus) {
            score += numbers[nextPosition.x][nextPosition.y]
        } else {
            score -= numbers[nextPosition.x][nextPosition.y]
        }
        pathPosition = nextPosition
    })
    document.querySelector('#score').innerHTML = score

    if(!robotPlaying && currentPosition.x === width - 1 && currentPosition.y === height - 1 && score === targetScore) {
        setTimeout(() => {
            alert("Congratulations! You won!")
            reset()
        }, 50)

    }
}

function positionToString(type, x, y) {
    return `${type}_x${x}y${y}`
}

function createRandomPath() {
    robotPlaying = true;
    while(currentPosition.x !== width - 1 || currentPosition.y !== height - 1) {
        const randomDir = Math.floor(Math.random() * 4)
        move(randomDir)
    }
    targetScore = score;
    document.querySelector('#target-score').innerHTML = targetScore
    robotPlaying = false;
}

function initialize() {
    currentPosition.x = 0
    currentPosition.y = 0
    for(let i = 0; i < width; i++) {
        numbers.push([]);
        for(let k = 0; k < height; k++) {
            const randomNumber = Math.floor(Math.random() * modulo)
            numbers[i].push(randomNumber)
        }
    }
    for(let i = 0; i < 2 * width - 1; i++) {
        operators.push([]);
        for(let k = 0; k < 2 * height - 1; k++) {
            if(i % 2 === 0 && k % 2 === 0) { // number
                operators[i].push(operator.nothing)
            } else if(i % 2 === 1 && k % 2 === 1) { // empty
                operators[i].push(operator.nothing)
            } else {
                const randomOperator = Math.floor(Math.random() * (Object.keys(operator).length - 1))
                operators[i].push(randomOperator)
            }
        }
    }
    for(let i = 0; i < width; i++) {
        visited.push([]);
        for(let k = 0; k < height; k++) {
            visited[i].push(false)
        }
    }
    visited[0][0] = true;
}

function clear() {
    path.length = 0;
    currentPosition.x = 0
    currentPosition.y = 0
    visited.forEach((column, xIndex) => {
        column.forEach((cell, yIndex) => {
            visited[xIndex][yIndex] = false;
        })
    })
    visited[0][0] = true;
    draw()
}

function reset() {
    window.location.reload(false)
}

initialize()
firstDraw()
createRandomPath()
clear();